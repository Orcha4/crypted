/**************************************************************************
 *   caesar.h  --  This file is part of Crypted.                          *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _CAESAR_H
#include "caesar.h"
#endif

caesar::caesar(std::basic_string<char> &input, int &shift)
{
    this->input = input;
    this->temp = input;
    this->shift_count = shift;
}

void caesar::encrypt()
{
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;
    int &shift_count = this->shift_count;

    for (size_t var = 0;var < input.size();++var)
        if ( isalpha(input[var]) )
            if ( (temp[var] + shift_count) > 122 )
                input[var] = ((temp[var] - 25) + (shift_count - 1));
            else
                input[var] = ((temp[var] + shift_count));
        else
            continue;
}

void caesar::decrypt()
{
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;
    int &shift_count = this->shift_count;

    for (size_t var = 0;var < input.size();++var)
        if ( isalpha(input[var]) )
            if ( (temp[var] - shift_count) < 97 )
                input[var] = ((temp[var] + 25) - (shift_count + 1) + 2);
            else
                input[var] = ((temp[var] - shift_count));
        else
            continue;
}

std::basic_string<char> caesar::get_input() const
{
    return this->input;
}
