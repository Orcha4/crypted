/**************************************************************************
 *   affine.cpp  --  This file is part of Crypted.                        *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _AFFINE_H
#include "affine.h"
#endif

affine::affine(std::basic_string<char> &input, int &a, int &b)
{
    this->input = input;
    this->temp = this->input;
    for (size_t i = 0;i < this->input.size();++i)
        if ( isalpha(this->input[i]) )
        {
            if ( islower(this->input[i]) )
                this->input[i] = toupper(this->temp[i]);
        }
        else
            continue;
    this->temp = this->input;
    this->a = a;
    this->b = b;
}

void
affine::encrypt()
{
    int &a = this->a;
    int &b = this->b;
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->input;

    for (size_t i = 0;i < input.size();++i)
        if ( isalpha(input[i]) )
            input[i] = ((a * (temp[i] - CODE) + b) % ALPHABET) + CODE;
        else
            continue;

    for (size_t i = 0;i < input.size();++i)
        if ( isupper(input[i]) )
        {
            input[i] = tolower(input[i]);
            temp[i] = input[i];
        }
}

void
affine::decrypt()
{
    int &a = this->a;
    int &b = this->b;
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;

    int flag, a_inv;

    for (int i = 0;i < ALPHABET;++i)
    {
        flag = (a * i) % ALPHABET;
        if ( flag == 1 )
            a_inv = i;
    }

    for (size_t i = 0;i < input.size();++i)
        if ( isalpha(input[i]) )
            input[i] = (a_inv * ( (temp[i] + CODE) - b ) % ALPHABET) + CODE;
        else
            continue;
    for (size_t i = 0;i < input.size();++i)
        if ( isupper(input[i]) )
        {
            input[i] = tolower(input[i]);
            temp[i] = input[i];
        }
}

std::basic_string<char>
affine::get_input() const
{
    return this->input;
}
